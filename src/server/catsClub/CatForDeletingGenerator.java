package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;

public class CatForDeletingGenerator extends AbstractResponseGenerator {
    @Override
    public void generate(Request request, Response response) {

        CatDeleteGenerator.catDel(request, Config.getCatDAO(), Config.getCatList());
        response.htmlText = Config.getCatManager().listCat(Config.getCatList().getCats());

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");
        response.headers.put("Location: ", "/jscats");

        response.htmlText = "<script src=\"/file/catstablebuilder.js\";></script>" +
                "<p>" +
                "<a href=\"http://localhost/addfarm\"><button type=\"button\" name=\"Add Cat\" >Add Cat</button></a>\n" +
                "</p>";

//                "<h1>List of Cats</h1>" +
//                        "<form method=\"get\" action=\"list\">" +
//                        response.htmlText +
//
//                        "<p>" +
//                        "       <a href=\"addfarm\"><button type=\"button\" name=\"Add Cat\" >Add Cat</button></a>\n" +
//                        "       <input value=\"Delete\" type=\"submit\" formaction=\"del\">" +
//                        "       <input value=\"Edit\" type=\"submit\" formaction=\"editfarm\">" +
//                        "</p>" +
//                        "</form>";

        response.setFileBody(response.htmlText.getBytes());

    }
}
