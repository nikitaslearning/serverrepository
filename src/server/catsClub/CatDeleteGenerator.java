package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;
import server.catsClub.dao.CatDAO;

public class CatDeleteGenerator extends AbstractResponseGenerator {
    @Override
    public void generate(Request request, Response response) {

    }

    public static void catDel (Request request, CatDAO catDAO, CatList catList){
        int id;
        int nose;
        nose = request.getPath().indexOf("cat=") +4;
        id = Integer.parseInt(request.getPath().substring(nose));
        catDAO.delCat(catList.getCatFromId(id));
        catList.delCat(id);
    }
}
