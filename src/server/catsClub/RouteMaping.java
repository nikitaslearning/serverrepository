package server.catsClub;

import server.AbstractResponseGenerator;

public class RouteMaping {
    public String route;
    public AbstractResponseGenerator generator;

    public RouteMaping(String route, AbstractResponseGenerator generator) {
        this.route = route;
        this.generator = generator;
    }
}
