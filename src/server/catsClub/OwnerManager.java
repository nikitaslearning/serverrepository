package server.catsClub;

import java.util.Scanner;


public class OwnerManager {

   public Owner owners[] = new Owner[100];

    public void console() {

        int i = 3;

//        owners[0] = new archive.Owner("karl", 514583);
//        owners[1] = new archive.Owner("semen", 887799);
//        owners[2] = new archive.Owner("fill", 123456);

        Scanner in = new Scanner(System.in);
        boolean exit = false;

        do {
            exit = true;
            System.out.println("Выберите действие");
            System.out.println("1.Добавить владельца");
            System.out.println("2.Удалить владельца");
            System.out.println("3.Вывести список владельцев");
            System.out.println("4.Выход");
            int reaction = in.nextInt();
//Добавлялка-----------------------------------------------------------------------------------
            if (reaction == 1) {
                owners[i] = addOwner();
                i++;
                exit = false;
            }
//Удалялка--------------------------------------------------------------------------------------
            if (reaction == 2) {
                if (i == 0) {
                    System.out.println("Владельцев нет, удалять нечего.");
                } else {
                    for (int p = 0; p < owners.length; p++) {
                        System.out.println(p + 1 + ". " + owners[p].name);
                        if (owners[p + 1] == null) {
                            p = owners.length;
                        }
                    }

                    System.out.println("=============    Укажите владельца для удаления");
                    int dellOwnerReaction = in.nextInt();
                    if (dellOwnerReaction < 1 && dellOwnerReaction > i) {
                        System.out.println("Введите правильное значение");
                    } else {
                        owners[dellOwnerReaction - 1] = null;
                        for (int z = dellOwnerReaction - 1; z < owners.length; z++) {
                            if (owners[z + 1] == null) {
                                break;
                            } else {
                                owners[z] = owners[z + 1];
                                owners[z + 1] = null;
                            }
                        }
                        i--;
                    }
                }
                exit = false;
            }

//Выведение списка-------------------------------------------------------------------------------
            if (reaction == 3) {
                if (i == 0) {
                    System.out.println("Список пуст");
                } else {

                    for (int p = 0; p < owners.length; p++) {
                        System.out.println(owners[p].name);
                        if (owners[p + 1] == null) {
                            p = owners.length;
                        }
                    }
                }
                System.out.println("----------------------");
                System.out.println();
                exit = false;
            }
//Выход
            if (reaction == 4) {
                exit = true;
            }
//Ошибка некоректного ввода

            if (reaction > 4 || reaction < 1) {
                System.out.println("Введите правильно пункт меню!");
                exit = false;
            }
        } while (exit == false);
    }


    //Добавлялка владельцев
    public Owner addOwner() {

        System.out.println("Введите номер телефона");
        Scanner in = new Scanner(System.in);
        int phone = in.nextInt();
        System.out.println("Введите имя");
        String name = in.next();
        Owner owner = new Owner(name, phone);
        return owner;
    }
}
