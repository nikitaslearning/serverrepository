package server.catsClub;

import server.catsClub.dao.CatDAO;
import server.catsClub.dao.CatDAOBaseSQLImpl;
import server.catsClub.dao.CatDAOMultiFileImpl;

import java.io.File;

public class Config {

    public final static File imgPath = new File("C:\\Users\\Чёрнаямолния\\Desktop\\Java\\Server\\src\\server\\img");

    private static CatDAO catDAO;

    public static CatDAO getCatDAO() {
        if (catDAO == null) {
//            catDAO = new CatDAOMultiFileImpl(new File("C:\\Users\\Чёрнаямолния\\Desktop\\Java\\Server\\src\\server\\dataCats"));
            catDAO = new CatDAOBaseSQLImpl();
        }
        return catDAO;
    }


    private static CatManager catManager;

     public static CatManager getCatManager() {
        if (catManager == null) {
            catManager = new CatManager();
        }
        return catManager;
    }

    private static CatList catListInstance;

     public static CatList getCatList() {
        if (catListInstance == null) {
            catListInstance = new CatList(getCatDAO());
        }
        return catListInstance;
    }

}
