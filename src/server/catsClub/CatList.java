package server.catsClub;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import server.Request;
import server.catsClub.dao.CatDAO;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.LinkedList;

public class CatList {


    public LinkedList<Cat> cats;

    public CatList(CatDAO catDAO) {
        cats = catDAO.loadCats();
    }

    public void addCat(Cat cat) {
        cats.add(cat);
    }

    public void delCat(int key) {
        for (Cat cat : cats) {
            if (cat.id == key) {
                cats.remove(cat);
                break;
            }
        }
    }

    public LinkedList<Cat> getCats() {
        return cats;
    }

    public Cat getCatFromId(int id) {
        Cat cat = null;
        for (Cat c : cats) {
            if (c.id == id) {
                cat = c;
                break;
            }
        }
        return cat;
    }

    public void editCat(int key, Cat editedCat) {

        int count = 0;
        for (Cat c : cats) {
            if (c.id == key) {
                editedCat.id = c.id;
                cats.set(count, editedCat);
                break;
            }
            count++;
        }
    }

    public String getCatsXML() {
        StringWriter sw = new StringWriter();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // root element
            Element rootElement = doc.createElement("cats");
            doc.appendChild(rootElement);

            for (int i = 0; i < cats.size(); i++) {
                Cat cat = cats.get(i);
                // cat element
                Element catElem = doc.createElement("cat");
                rootElement.appendChild(catElem);

                // catname element
                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(Integer.toString(cat.id)));
                Element catname = doc.createElement("catname");
                catname.appendChild(doc.createTextNode(cat.name));
                Element imgname = doc.createElement("imgname");
                imgname.appendChild(doc.createTextNode(cat.imgName));
                Element weight = doc.createElement("weight");
                weight.appendChild(doc.createTextNode(Integer.toString(cat.weight)));
                Element height = doc.createElement("height");
                height.appendChild(doc.createTextNode(Integer.toString(cat.height)));
                Element length = doc.createElement("length");
                length.appendChild(doc.createTextNode(Integer.toString(cat.length)));

                catElem.appendChild(id);
                catElem.appendChild(catname);
                catElem.appendChild(imgname);
                catElem.appendChild(weight);
                catElem.appendChild(height);
                catElem.appendChild(length);

                rootElement.appendChild(catElem);
            }

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sw.toString();
    }

}


// ---------------------   Array version
//    public Cat[] cats;
//
//    public CatList(CatDAO catDAO) {
//        cats = catDAO.loadCats();
//    }
//
//    public void addCat(Cat cat) {
//        Cat[] buffer = new Cat[cats.length + 1];
//        System.arraycopy(cats, 0, buffer, 0, cats.length);
//        buffer[cats.length] = cat;
//        cats = buffer;
//    }
//
//    public void delCat(int key) {
//        int id = 0;
//        for (int i = 0; i < cats.length; i++) {
//            if (cats[i].id == key){
//                id = cats[i].id;
//            }
//        }
//
//        for (int i = id; i != cats.length - 1; i++) {
//            cats[i] = cats[i + 1];
//        }
//        Cat[] buffer = new Cat[cats.length - 1];
//        System.arraycopy(cats, 0, buffer, 0, buffer.length);
//        cats = buffer;
//    }
//
//    public Cat[] getCats() {
//        return cats;
//    }