package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;

 public class CatGenerator extends AbstractResponseGenerator {

    @Override
    public void generate(Request request, Response response) {

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");

        response.htmlText =

                "<h1>Cat Cat Cat</h1>\n" +
                        "<p style=\"font-size: 30px\">Grigory</p>" +
                        "<img src=\"http://localhost/file/cat2.jpg\"" +
                        "height=\"255\">" +

                        "<p style=\"font-size: 30px\">Inakentiy</p>" +
                        "<img src=\"http://localhost/file/cat1.jpg\"" +
                        "height=\"255\">" +

                        "<h1>Time</h1>\n" +
                        "<p style=\"font-size: 30px\">" + response.getDate("time") + "</p>" +

                        "<p>Попробуйте перейти на <a href=\"http://localhost/person\">главную страницу Person's</a> и найти нужную вам статью через форму поиска.</p>";


            response.setFileBody(response.htmlText.getBytes());
    }

}

