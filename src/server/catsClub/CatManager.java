package server.catsClub;

import server.Request;
import server.catsClub.dao.CatDAO;

import java.util.LinkedList;
import java.util.Scanner;


public class CatManager {


    public Cat cats[] = new Cat[100];
    public int size = 0;

    public void console(CatList catList, CatDAO catDAO) {

        Scanner in = new Scanner(System.in);
        boolean exit = false;

        do {
            exit = true;
            System.out.println("Выберите действие");
            System.out.println("1.Добавить кота");
            System.out.println("2.Удалить кота");
            System.out.println("3.Вывести список");
            System.out.println("4.Выход");
            int reaction = in.nextInt();
//Добавлялка
            if (reaction == 1) {
                catList.addCat(addCat(catDAO));
                size++;
                exit = false;
            }
//Удалялка
            if (reaction == 2) {
                if (catList.cats.size() == 0) {
                    System.out.println("Котов нет, удалять нечего.");
                } else {
                    for (int p = 0; p < catList.cats.size(); p++) {
                        System.out.println(p + 1 + ". " + catList.cats.get(p).name);
                        if (p + 1 == catList.cats.size()) {
                            break;
                        }
                    }
                    System.out.println("=============    Укажите кота для удаления");
                    int dellOwnerReaction = in.nextInt();
                    if (dellOwnerReaction < 1 && dellOwnerReaction > catList.cats.size()) {
                        System.out.println("Введите правильное значение");
                    } else {
                        catDAO.delCat(catList.cats.get(dellOwnerReaction - 1));
                        catList.cats.remove(dellOwnerReaction - 1);

                        size--;
                    }
                }
                exit = false;
            }
//Выведение списка
            if (reaction == 3) {
                if (catList.cats.size() == 0) {
                    System.out.println("Список пуст");
                } else {

                    for (int p = 0; p < catList.cats.size(); p++) {
                        System.out.println(catList.cats.get(p).name);
                        if (p + 1 == catList.cats.size()) {
                            break;
                        }
                    }
                }
                System.out.println("----------------------");
                System.out.println();
                exit = false;
            }

//Выход
            if (reaction == 4) {
                exit = true;
            }
//Ошибка некоректного ввода

            if (reaction > 4 || reaction < 1) {
                System.out.println("Введите правильно пункт меню!");
                exit = false;
            }
        } while (exit == false);

    }


    //Добавлялка котов
    public Cat addCat(CatDAO catDAO) {

        System.out.println("Введите длинну");
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        System.out.println("Введите ширину");
        int weight = in.nextInt();
        System.out.println("Введите высоту");
        int height = in.nextInt();
        System.out.println("Введите имя");
        String name = in.next();
        System.out.println("Введите имя картинки кота");
        String imgName = in.next();

        Cat cat = new Cat(length, weight, height, name, imgName);
//        CatDAOMultiFileImpl catDAO = new CatDAOMultiFileImpl(new File("E:\\img\\cats"));
        catDAO.addCat(cat);

        return cat;
    }

    public Cat addCat(CatDAO catDAO, Request request) {
        String path = request.getPath();
        int indexNose;
        int indexTail;

        int length, weight, height;
        String name, imgName;

        indexNose = path.indexOf("length=") + 7;
        indexTail = path.indexOf("&weight=");
        length = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 8;
        indexTail = path.indexOf("&height=");
        weight = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 8;
        indexTail = path.indexOf("&name=");
        height = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 6;
        indexTail = path.indexOf("&imgName=");
        name = path.substring(indexNose, indexTail);
        indexNose = indexTail + 9;
        indexTail = path.indexOf("&Submit=");
        imgName = path.substring(indexNose, indexTail);

        Cat cat = new Cat(length, weight, height, name, imgName);

        catDAO.addCat(cat);

        return cat;
    }

    public Cat parseCatParams(Request request) {
        String path = request.getPath();
        int indexNose;
        int indexTail;

        int length, weight, height;
        String name, imgName;

        indexNose = path.indexOf("length=") + 7;
        indexTail = path.indexOf("&weight=");
        length = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 8;
        indexTail = path.indexOf("&height=");
        weight = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 8;
        indexTail = path.indexOf("&name=");
        height = Integer.parseInt(path.substring(indexNose, indexTail));
        indexNose = indexTail + 6;
        indexTail = path.indexOf("&imgName=");
        name = path.substring(indexNose, indexTail);
        indexNose = indexTail + 9;
        indexTail = path.indexOf("&cat=");
        imgName = path.substring(indexNose, indexTail);

        Cat cat = new Cat(length, weight, height, name, imgName);

        return cat;
    }

    public String listCat(LinkedList<Cat> cats) {
        StringBuilder sb = new StringBuilder();

        if (cats.isEmpty()) {
            sb.append("List is empty");
        } else {
            int p;
            sb.append("<table border=\"4px\">\n");

            for (p = 0; p < cats.size(); p++) {
                sb.append("<tr>\n" +
                          "<td align=\"center\">"+cats.get(p).id+"</td><td>"+cats.get(p).name+
                        "</td><td>"+cats.get(p).length+" x "+cats.get(p).weight+" x "+cats.get(p).height+"</td>" +
                        "<td align=\"center\"><img src=\"http://localhost/file/"+cats.get(p).imgName+"\"</td>" +
                        "<td><button type=\"submit\" value="+cats.get(p).id + " name=\"cat\" formaction=\"http://localhost/editform\">Edit</button>" +
                        "<button type=\"submit\" value="+cats.get(p).id+" name=\"cat\" formaction=\"del\">Delete</button>"+
                        "</tr>" +
                        "</p>"
                );
                if (p + 1 == cats.size()) {
                    break;
                }
            }
            sb.append("</table>");
//            System.out.println("The request is generated");
        }

        return sb.toString();
    }

    public void delCat(CatList catList, CatDAO catDAO, Request request) {


    }

    public void exit() {

    }
}
