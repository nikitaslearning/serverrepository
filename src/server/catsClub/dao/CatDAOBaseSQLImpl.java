package server.catsClub.dao;


import com.sun.javafx.scene.control.skin.VirtualFlow;
import server.DBWorker;
import server.catsClub.Cat;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class CatDAOBaseSQLImpl implements CatDAO {
    DBWorker dbWorker = new DBWorker();

    @Override
    public void addCat(Cat cat) {
        String query = "INSERT INTO cats.catsi.ctasinfo(name, length, weight, height, imgpath) VALUES (?,?,?,?,?)";

        try {
            PreparedStatement ps = dbWorker.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, cat.name);
            ps.setInt(2, cat.length);
            ps.setInt(3, cat.weight);
            ps.setInt(4, cat.height);
            ps.setString(5, cat.imgName);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            cat.id = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LinkedList<Cat> loadCats() {

        String query = "SELECT * FROM cats.catsi.ctasinfo";
        LinkedList<Cat> catLinkedList = null;
        try {
            Statement statement = dbWorker.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);

            catLinkedList = new LinkedList<>();
            int count = 0;
            while(rs.next()){
                catLinkedList.add(loadCat(rs));
                count++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return catLinkedList;
    }

    public Cat loadCat(ResultSet cat){
        Cat c = null;
        try {
            c = new Cat(cat.getInt(1), cat.getInt(3), cat.getInt(4),
                        cat.getInt(5),cat.getString(2),cat.getString(6));
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Cat read error");
        }
        return c;
    }

    @Override
    public void delCat(Cat cat) {
        String query = "DELETE FROM cats.catsi.ctasinfo WHERE id = " + cat.id;

        try {
            PreparedStatement ps = dbWorker.getConnection().prepareStatement(query);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editCat(int catKey, Cat editedCat){

        String query = "UPDATE cats.catsi.ctasinfo " +
                "SET " +
                " name = ?," +
                " length = ?," +
                " weight = ?," +
                " height = ?," +
                " imgpath = ?" +
                "WHERE id = ?";
        try {
            PreparedStatement ps = dbWorker.getConnection().prepareStatement(query);

            ps.setString(1, editedCat.name);
            ps.setInt(2, editedCat.length);
            ps.setInt(3, editedCat.weight);
            ps.setInt(4, editedCat.height);
            ps.setString(5, editedCat.imgName);
            ps.setInt(6, catKey);

            ps.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
