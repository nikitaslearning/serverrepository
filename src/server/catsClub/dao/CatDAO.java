package server.catsClub.dao;


import server.Request;
import server.catsClub.Cat;

import java.util.LinkedList;

public interface CatDAO {

    void addCat(Cat cat);

    LinkedList<Cat> loadCats();

    void delCat(Cat cat);

    void editCat(int id, Cat cat);

}
