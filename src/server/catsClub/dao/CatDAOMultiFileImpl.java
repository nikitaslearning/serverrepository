package server.catsClub.dao;

import server.catsClub.Cat;

import java.io.*;
import java.util.LinkedList;

public class CatDAOMultiFileImpl implements CatDAO {

    File fileCats;

    public CatDAOMultiFileImpl(File fileCats) {

        this.fileCats = fileCats;
    }

    public void addCat(Cat cat) {

        String catValues;

        try {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(fileCats, cat.name + ".txt")));
            catValues = cat.length + "\n" + cat.weight + "\n" + cat.height + "\n" + cat.name + "\n" + cat.imgName + "\n";
            try {
                dos.writeBytes(catValues);

                dos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public LinkedList<Cat> loadCats() {

        LinkedList<Cat> cats = new LinkedList<>();
        int catsArrayLength = 0;
        File[] files = fileCats.listFiles();

        if (files.length == 0) {
            System.out.println();
            System.out.println("Нет сохранённых котов!");
            System.out.println("------------------------------------");
        } else {
            int numberOfCats = files.length;

            for (int i = 0; i < numberOfCats; i++) {
                cats.add(loadCat(files[i]));
            }
        }
        return cats;
    }

    static Cat loadCat(File file) {

        String[] catParams = new String[5];
        DataInputStream din = null;
        try {
            din = new DataInputStream(new FileInputStream(new File(file.getAbsolutePath())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte b;
            byte[] buffer = new byte[100];
            int bufferPoint = 0;
            int paramsPoint = 0;

            while (din.available() > 0) {
                b = din.readByte();
                if (b == 13){
                    b = din.readByte();
                }
                if (b == 10) {
                    byte[] value = new byte[bufferPoint];
                    System.arraycopy(buffer, 0, value, 0, bufferPoint);
                    buffer = new byte[100];
                    catParams[paramsPoint] = new String(value);
                    int test = Integer.parseInt(catParams[0]);
                    bufferPoint = 0;
                    paramsPoint++;

                } else {
                    buffer[bufferPoint] = b;
                    bufferPoint++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            din.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Cat cat = new Cat(Integer.parseInt(catParams[0]), Integer.parseInt(catParams[1]), Integer.parseInt(catParams[2]), catParams[3], catParams[4]);
        return cat;
    }

    public void delCat(Cat cat) {

        try {

            File file = new File(fileCats, cat.name + ".txt");

            if (file.delete()) {
                System.out.println("Кот " + file.getName() + " удалён!");
                System.out.println("------------------------------------");
                System.out.println();
            } else {
                System.out.println("This file not found and can not be deleted!");
                System.out.println("---------------------------------------------");
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editCat(int id, Cat cat) {
//---------------------------not implement for console-------------
        System.out.println("not implement for console");
        System.out.println("not implement for console");
        System.out.println("not implement for console");
    }
}
