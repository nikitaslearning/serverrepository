package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;
import server.catsClub.dao.CatDAO;

/**
 * Created by n.bashkin on 10.11.2017.
 */
public class CatEditFormGenerator extends AbstractResponseGenerator {
    @Override
    public void generate(Request request, Response response) {
        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");

        response.htmlText =

                " <h1>Enter parameters of new cat </h1>" +
                        "<form method=\"get\" action=\"lockalhost\">" +

                        "<p><b>Length&nbsp;</b><input type=\"text\" size=\"2\" name=\"length\" required style=\"margin-right:10px\"></p>" +
                        "<p><b>Weight&nbsp;</b><input type=\"text\" size=\"2\" name=\"weight\" required style=\"margin-right:10px\"></p>" +
                        "<p><b>Height&nbsp;</b><input type=\"text\" size=\"2\" name=\"height\" required style=\"margin-right:10px\"></p>" +
                        "<p><b>Name&nbsp;</b><input type=\"text\" size=\"10\" name=\"name\" required style=\"margin-right:10px\"></p>" +
                        "<p><b>imgName&nbsp;</b><input type=\"text\" size=\"10\" name=\"imgName\" required style=\"margin-right:10px\"></p>" +
                        "<input type=\"hidden\" size=\"10\" name=\"cat\" value=" + catValue(request)+  "></p>" +
                        "<p><input name=\"Edit\" type=\"submit\"  formaction=\"editcat\"></p>" +

                        "</form>";

        response.setFileBody(response.htmlText.getBytes());

    }
//    Исправить позднее(как много туда-сюда всего с value)_______________________________________________________________

    private static int catValue(Request request) {
        int id;
        int nose;
        int tail;
        nose = request.getPath().indexOf("cat=") + 4;
        if (request.getPath().startsWith("editform")){
            tail = request.getPath().indexOf(nose);
            id = Integer.parseInt(request.getPath().substring(nose));
        }else {
            tail = request.getPath().indexOf("&Edit", nose);
            id = Integer.parseInt(request.getPath().substring(nose,tail));
        }


        return id;
    }

    public static void editCat(Request request, CatDAO catDAO, CatList catList, CatManager catManager) {
        int id = catValue(request);
        Cat editedCat = catManager.parseCatParams(request);

        catDAO.editCat(id, editedCat);
        catList.editCat(id, editedCat);
    }
}
