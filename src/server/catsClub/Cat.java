package server.catsClub;


import server.Request;
import server.Response;

public class Cat {

    public int id, length, weight, height;
    public String name, imgName;

    public Cat(int id, int length, int weight, int height, String name, String imgName) {
        this.id = id;
        this.length = length;
        this.weight = weight;
        this.height = height;
        this.name = name;
        this.imgName = imgName;
    }
    public Cat(int length, int weight, int height, String name, String imgName) {
        this.length = length;
        this.weight = weight;
        this.height = height;
        this.name = name;
        this.imgName = imgName;
    }

    public int volume() {
        int s;
        s = this.length * this.weight * this.height;
        return s;
    }

}
