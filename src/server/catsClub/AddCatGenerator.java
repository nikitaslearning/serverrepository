package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;

public class AddCatGenerator extends AbstractResponseGenerator {


    @Override
    public void generate(Request request, Response response) {

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");

        response.htmlText =

                " <h1>Enter parameters of new cat </h1>" +
                        "<form method=\"get\" action=\"addcat\">" +


                        "<p><b>Length&nbsp;</b><input type=\"text\" size=\"2\" name=\"length\" required></p>" +
                        "<p><b>Weight&nbsp;</b><input type=\"text\" size=\"2\" name=\"weight\" required></p>" +
                        "<p><b>Height&nbsp;</b><input type=\"text\" size=\"2\" name=\"height\" required></p>" +
                        "<p><b>Name&nbsp;</b><input type=\"text\" size=\"10\" name=\"name\" required></p>" +
                        "<p><b>imgName&nbsp;</b><input type=\"text\" size=\"10\" name=\"imgName\" required></p>" +
                        "<p><input name=\"Submit\" type=\"submit\"></p>" +

                        "</form>";

        response.setFileBody(response.htmlText.getBytes());
    }
}
