package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;

public class ListGenerator extends AbstractResponseGenerator {


    @Override
    public void generate(Request request, Response response) {

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");
        response.headers.put("Access-Control-Allow-Origin: ", "*");

        response.htmlText =

                "<h1>List of Cats</h1>" +
                        "<form method=\"get\" action=\"list\">" +
                        response.htmlText +

                        "<p>" +
                        "<a href=\"http://localhost/addfarm\"><button type=\"button\" name=\"Add Cat\" >Add Cat</button></a>\n" +
                        "</p>" +
                        "</form>";

        response.setFileBody(response.htmlText.getBytes());
    }
}
