package server.catsClub;

import server.AbstractResponseGenerator;
import server.Request;
import server.Response;

public class CatEditGenerator extends AbstractResponseGenerator {
    @Override
    public void generate(Request request, Response response) {

        CatEditFormGenerator.editCat(request, Config.getCatDAO(), Config.getCatList(), Config.getCatManager());
        response.htmlText = Config.getCatManager().listCat(Config.getCatList().getCats());


        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(302);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");
        response.headers.put("Location: ", "/jscats");

        response.htmlText = "";

        response.setFileBody(response.htmlText.getBytes());

    }
}
