// 1 запросить xml с котами
var xhr = new XMLHttpRequest();
xhr.open("GET", "http://localhost/xmlcatslist.XML", false);
xhr.send(null);
var pageName = document.createElement("h1");
var pnText = document.createTextNode("JS Cats list");
pageName.appendChild(pnText);
document.body.appendChild(pageName);
// 2 распарсить и нарисовать
if (xhr.status == 200) {
// превращаем полученный xml в список
    xmlDoc = xhr.responseXML;
    var cats = xmlDoc.getElementsByTagName("cat");
// создаём таблицу
    var form = document.createElement("FORM");
    form.setAttribute("id", "catfForm");
    form.setAttribute("name", "form");
    document.body.appendChild(form);
    var cattable = document.createElement("TABLE");
    cattable.setAttribute("id", "catsTableID");
    cattable.setAttribute("border", "4px");
    form.appendChild(cattable);
// записываем строки в таблицу(каждая строка один кот)
    for (i = 0; i < cats.length; i++) {
        var id = cats[i].childNodes[1].textContent;
        var name = cats[i].childNodes[3].textContent;
        var imgname = cats[i].childNodes[5].textContent;
        var weight = cats[i].childNodes[7].textContent;
        var height = cats[i].childNodes[9].textContent;
        var length = cats[i].childNodes[11].textContent;

        var y = document.createElement("TR");
        y.setAttribute("id", "myTr" + i);
        document.getElementById("catsTableID").appendChild(y);

        var z1 = document.createElement("TD");
        var t1 = document.createTextNode(id);
        z1.appendChild(t1);
        var z2 = document.createElement("TD");
        var t2 = document.createTextNode(name);
        z2.appendChild(t2);

        var z3 = document.createElement("TD");
        var t3 = document.createTextNode(weight + "x" + height + "x" + length);
        z3.appendChild(t3);

        var z4 = document.createElement("TD");
        z4.setAttribute("align", "center");
        var i4 = document.createElement("img");
        i4.setAttribute("src", "http://localhost/file/" + imgname);
        z4.appendChild(i4);

        var z5 = document.createElement("TD");
// Edit button
        var be5 = document.createElement("button");
        be5.setAttribute("type", "submit");
        be5.setAttribute("value", id);
        be5.setAttribute("name", "cat");
        be5.setAttribute("formaction", "http://localhost/editform");
        var beName = document.createTextNode("Edit");
        be5.appendChild(beName);
        z5.appendChild(be5);
// Delete button
        var bd5 = document.createElement("button");
        bd5.setAttribute("type", "submit");
        bd5.setAttribute("value", id);
        bd5.setAttribute("name", "cat");
        bd5.setAttribute("formaction", "del");
        var bdName = document.createTextNode("Delete");
        bd5.appendChild(bdName);
        z5.appendChild(bd5);


        document.getElementById("myTr" + i).appendChild(z1);
        document.getElementById("myTr" + i).appendChild(z2);
        document.getElementById("myTr" + i).appendChild(z3);
        document.getElementById("myTr" + i).appendChild(z4);
        document.getElementById("myTr" + i).appendChild(z5);

    }
}


