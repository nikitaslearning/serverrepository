package server;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import server.catsClub.*;
import server.catsClub.dao.CatDAO;
import server.catsClub.dao.CatDAOMultiFileImpl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ForTests {

    public static void main(String[] args) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(new File("C:\\Users\\Чёрнаямолния\\Desktop\\recording_20studio_20list.xml"));


            DBWorker dbWorker = new DBWorker("recordingstudios");
            Element documentElement = doc.getDocumentElement();
            NodeList workBookNodes = documentElement.getChildNodes();
            for (int i = 0; i < workBookNodes.getLength(); i++) {
                if (workBookNodes.item(i).getNodeName().equals("Worksheet")) {
                    NamedNodeMap wSAttributes = workBookNodes.item(i).getAttributes();
                    if (wSAttributes.getNamedItem("ss:Name").getNodeValue().equals("Sheet1")) {
                        NodeList workSheet1ChildNodes = workBookNodes.item(i).getChildNodes();
                        for (int z = 0; z < workSheet1ChildNodes.getLength(); z++) {
                            if (workSheet1ChildNodes.item(z).getNodeName().equals("Table")) {
                                NodeList tableChildNodes = workSheet1ChildNodes.item(z).getChildNodes();
                                boolean headline = true;
                                boolean headlineAdd;
                                try {
                                    String query = "SELECT * FROM recordingstudios.studios.tableofstudios";
                                    Statement stmnt = dbWorker.getConnection().createStatement();
                                    stmnt.executeQuery(query);
                                    headlineAdd = false;
                                } catch (SQLException exc) {
                                    headlineAdd = true;
                                }
                                int columns = 0;
                                StringBuilder sbCreateTable = new StringBuilder();
                                StringBuilder sbParamsNose = new StringBuilder();
                                StringBuilder sbValuesTail = new StringBuilder();
                                for (int t = 0; t < tableChildNodes.getLength(); t++) {
                                    if (tableChildNodes.item(t).getNodeName().equals("Row")) {
                                        NodeList cellNodes = tableChildNodes.item(t).getChildNodes();
                                        if (headline) {
                                            sbCreateTable.append("CREATE TABLE IF NOT EXISTS recordingstudios.studios.tableofstudios(");
                                            sbParamsNose.append("INSERT INTO recordingstudios.studios.tableofstudios(");
                                            sbValuesTail.append("VALUES(");
                                            for (int c = 1; c < cellNodes.getLength(); c += 2) {
                                                if (c < cellNodes.getLength() - 2) {
                                                    sbCreateTable.append("\"" + cellNodes.item(c).getTextContent().replaceAll("\\s+", "").toLowerCase() + "\" " + "varchar(100),");
                                                    sbParamsNose.append(cellNodes.item(c).getTextContent().replaceAll("\\s+", "").toLowerCase() + ",");
                                                    sbValuesTail.append("?,");
                                                } else {
                                                    sbCreateTable.append("\"" + cellNodes.item(c).getTextContent().replaceAll("\\s+", "").toLowerCase() + "\" " + "varchar(100));");
                                                    sbParamsNose.append(cellNodes.item(c).getTextContent().replaceAll("\\s+", "").toLowerCase() + ") ");
                                                    sbValuesTail.append("?);");
                                                }
                                                columns++;
                                            }
                                            sbParamsNose.append(sbValuesTail.toString());
                                            System.out.println(sbCreateTable.toString());
                                            if (headlineAdd) {
                                                String query = sbCreateTable.toString();
                                                PreparedStatement ps = dbWorker.getConnection().prepareStatement(query);
                                                ps.execute();
                                            }
                                            headline = false;
                                        } else {
                                            String query1 = sbParamsNose.toString();
                                            PreparedStatement ps = dbWorker.getConnection().prepareStatement(query1);
                                            int cellCount = 1;
                                            for (int parametr = 1; parametr < columns + 1; parametr++) {
                                                if (cellNodes.item(cellCount) == null) {
                                                    ps.setString(parametr, "????????????????????");
                                                } else {
                                                    ps.setString(parametr, cellNodes.item(cellCount).getTextContent());
                                                }
                                                cellCount += 2;
                                            }
                                            ps.execute();
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                    break;
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> readRow(NodeList row) {
        ArrayList<String> cells = new ArrayList<>();

        for (int i = 0; i < row.getLength(); i++) {
            if (row.item(i).getNodeName().equals("Cell")){
                cells.add(row.item(i).getTextContent().replaceAll("\\s+", "").toLowerCase());
            }
        }
        return cells;
    }

}
//        List<Cat> cats = Config.getCatList().getCats();
//        StringWriter sw = new StringWriter();
//        try {
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.newDocument();
//
//            // root element
//            Element rootElement = doc.createElement("cats");
//            doc.appendChild(rootElement);
//
//            for (int i = 0; i < cats.size(); i++) {
//                Cat cat = cats.get(i);
//                // cat element
//                Element catElem = doc.createElement("cat");
//                rootElement.appendChild(catElem);
//
//                // catname element
//                Element id = doc.createElement("id");
//                id.appendChild(doc.createTextNode(Integer.toString(cat.id)));
//
//                Element catname = doc.createElement("catname");
//                catname.appendChild(doc.createTextNode(cat.name));
//
//                Element imgname = doc.createElement("imgname");
//                imgname.appendChild(doc.createTextNode(cat.imgName));
//
//                Element weight = doc.createElement("weight");
//                weight.appendChild(doc.createTextNode(Integer.toString(cat.weight)));
//                Element height = doc.createElement("height");
//                height.appendChild(doc.createTextNode(Integer.toString(cat.height)));
//                Element length = doc.createElement("length");
//                length.appendChild(doc.createTextNode(Integer.toString(cat.length)));
//
//                catElem.appendChild(id);
//                catElem.appendChild(catname);
//                catElem.appendChild(imgname);
//                catElem.appendChild(weight);
//                catElem.appendChild(height);
//                catElem.appendChild(length);
//
//                rootElement.appendChild(catElem);
//            }
//
//            TransformerFactory tf = TransformerFactory.newInstance();
//            Transformer transformer = tf.newTransformer();
//            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
//            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
//            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//
//            transformer.transform(new DOMSource(doc), new StreamResult(sw));
//            System.out.println(sw.toString());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
////-----------------------Cats club----------------------
//        Scanner in = new Scanner(System.in);
//        boolean exit = true;
//        OwnerManager ownerManager = new OwnerManager();
//
//        CatManager catManager = new CatManager();
//
//        CatDAO catDAO = new CatDAOMultiFileImpl(new File("C:\\Users\\n.bashkin\\IdeaProjects\\nikgit\\src\\server\\dataCats"));
//        CatList catList = Config.getCatList();
//
//
//        do {
//
//// -------------------Menu console-----------------------
//            System.out.println("1.Управление владельцами");
//            System.out.println("2.Управление котами");
//            System.out.println("3.Закрепить кота за владельцем");
//            System.out.println("4.Вывести список принадлежности хозяин-коты");
//            System.out.println("5.Web version");
//            System.out.println("6.Выход");
//            int reaction = in.nextInt();
////Управление владельцами----------------------------------------------------------------------
//            if (reaction == 1) {
//                ownerManager.console();
//                exit = true;
//            }
////Управление котами-------------------------------------------------------------------------
//            if (reaction == 2) {
//                catManager.console(catList, catDAO);
//                exit = true;
//            }
////Закрепляем котов за владельцем--------------------------------------------------------------
//
//            if (reaction == 3) {
//                if (ownerManager.owners[0] == null) {
//                    System.out.println("Нет ни одного владельца");
//                } else {
//                    for (int i = 0; i < ownerManager.owners.length; i++) {
//                        System.out.println(i + 1 + ". " + ownerManager.owners[i].name);
//                        if (ownerManager.owners[i + 1] == null) {
//                            break;
//                        }
//                    }
//                    System.out.println();
//                    System.out.println("   Выбирите владельца коту");
//                }
//                int picOwner = in.nextInt() - 1;
//                for (int i = 0; i < catManager.cats.length; i++) {
//                    System.out.println(i + 1 + ". " + catManager.cats[i].name);
//                    if (catManager.cats[i + 1] == null) {
//                        break;
//                    }
//                }
//                System.out.println();
//                System.out.println("   Выбирите кота");
//                int picDog = in.nextInt() - 1;
//
//// Проверка принадлежности котов-------------------------------------------------------------
//// проверка по массиву котов каждого хозяина
//
//                boolean quit = true;
//                for (int i = 0; i < ownerManager.owners.length; i++) {
//                    if (ownerManager.owners[i + 1] == null) {
//                        break;
//
////тут что то со котами после ЭЛС
////    проверить позднее
//                    } else
//                        for (int i1 = 0; i1 < ownerManager.owners[i].cats.length; i1++) {
//                            if (catManager.cats[i1 + 1] == null) {
//                                break;
//                            } else if (catManager.cats[picDog] == ownerManager.owners[i].cats[i1]) {
//                                System.out.println("Кот принадлежит " + ownerManager.owners[i].name);
//                                quit = false;
//                            }
//                        }
//                }
//
//                if (quit == true) {
//                    ownerManager.owners[picOwner].addCat(catManager.cats[picDog]);
//
//                    System.out.println(ownerManager.owners[picOwner].name + " теперь хзяин " + catManager.cats[picDog].name);
//                }
//
//                exit = true;
//
//            }
////Список принадлежности хозяин-собаки--------------------------------------------------------
////ДОДЕЛАТЬ!!!!!!!!!!!!
//// Не работает! Подумать над вариантом решения с помощью Вайл
//// Доделать свободных собак
//            if (reaction == 4) {
//                for (int s = 0; s < ownerManager.owners.length; s++) {
//                    if (ownerManager.owners[0] == null) {
//                        System.out.println("Нет ниодного владельца");
//                        break;
//                    } else {
//                        if (ownerManager.owners[s].cats[0] == null) {
//                            System.out.println(ownerManager.owners[s].name + " не имеет собак");
//                        } else {
//                            System.out.println(ownerManager.owners[s].name + " является владельцем:");
//                            for (int s1 = 0; s1 < ownerManager.owners[s].cats.length; s1++) {
//                                System.out.println("- " + ownerManager.owners[s].cats[s1].name);
//                                if (ownerManager.owners[s].cats[s1 + 1] == null) {
//                                    break;
//                                }
//                            }
//                        }
//                        if (ownerManager.owners[s + 1] == null) {
//                            break;
//                        }
//                    }
//                }
//                System.out.println("----------------");
//                System.out.println("Свободные коты:");
//                String dogRes = "Нет ни одного коты";
//                if (catManager.cats.length == 0) {
//                    System.out.println(dogRes);
//                } else {
//                    for (int d = 0; d < catManager.cats.length; d++) {
//                        for (int d1 = 0; d1 < ownerManager.owners.length; d1++) {
//                            for (int d2 = 0; d2 < ownerManager.owners[d1].cats.length; d2++) {
//                                dogRes = ownerManager.owners[d1].cats[d2].name;
//                                if (catManager.cats[d] == ownerManager.owners[d1].cats[d2]) {
//                                    dogRes = "Нет ни одного кота";
//                                    d++;
//                                    break;
//                                }
//                                if (ownerManager.owners[d1].cats[d2 + 1] == null) {
//                                    break;
//                                }
//                            }
//                            if (ownerManager.owners[d1 + 1] == null) {
//                                break;
//                            }
//                        }
//                        if (catManager.cats[d + 1] == null) {
//                            break;
//                        }
//                        System.out.println(dogRes);
//                    }
//                    if (dogRes == "Нет ни одного кота") {
//                        System.out.println(dogRes);
//                    }
//                }
//                exit = true;
//            }
//
//
////Выход
//            if (reaction == 6) {
//                exit = false;
//                System.out.println("До свидания!");
//            }
//
////Ошибка--------------------------------------------------------------------------------------
//            if (reaction > 6 || reaction < 1) {
//                System.out.println("Введите правильно пункт меню!");
//                exit = false;
//            }
//
//
//// -------------------Menu console end-------------------
//
//
//        } while (exit);





/*

            CatDAOMultiFileImpl catDAO = new CatDAOMultiFileImpl(new File("E:\\img\\cats"));

            Cat[] cats = catDAO.loadCats();

                for (int i = 0; i < cats.length; i ++) {
                    catManager.cats[i] = cats[i];
                    catManager.size ++;
                }

 */




/*
мой вариант чтения из сокета

                    in.read(chars);
                    b = new String(chars);

                    if (in.available() <= chars.length) {
                        sb.append(b);
                        chars = new byte[in.available()];
                        in.read(chars);
                        b = new String(chars);
                        sb.append(b);
                        System.out.println(sb.toString());
                        exit = false;
                    }else {
                        sb.append(b);
                    }

 */


/*
figure

       FigureNotAbstract[] figures = new FigureNotAbstract[3];

        figures[0] = new Circle(50);
        figures[1] = new Rectangle(12, 22, 3, 17);
        figures[2] = new Rhomb(41, 35, 15, 1);

        for (int i = 0; i < figures.length; i++) {
            figures[i].printName();
            for (int i1 = 0; i1 < figures[i].getPoints().length; i1++) {
                System.out.println(figures[i].getPoints()[i1]);
            }
        }

 */
/*
        HashMap<String, String> hashMap = new HashMap<>();

        String path = "C:\\Users\\Чёрнаямолния\\Desktop\\Gen3\\secretFile[enc=win1251,param1=value1,param2=value2,stat=true].txt";

        HashMap<String, String> hm = new HashMap<>();

        hm = parseFileName(path);

        for (Map.Entry entry : hm.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }
        */


/*
    static HashMap parseFileName(String path) {

        HashMap<String, String> hm = new HashMap<>();

        int nose = path.indexOf("[");
        int tail = path.indexOf("]");
        String cropPath = path.substring(nose + 1, tail);
        String[] parametrs = cropPath.split(",");

        for (int i = 0; i < parametrs.length; i++) {

            String[] param = parametrs[i].split("=");
            hm.put(param[0], param[1]);
        }

        return hm;
    }
*/
    /*
     static HashMap parseFileName(String path) {

        HashMap<String, String> hm = new HashMap<>();
        int nose = path.indexOf("[");
        int tail = path.indexOf("=");
        String par1;
        String par2;
        boolean exit = true;
        while (exit) {

            par1 = path.substring(nose + 1, tail);
            nose = tail;
            if (tail != path.lastIndexOf("=")) {
                tail = path.indexOf(",", nose);
            } else {
                tail = path.indexOf("]");
            }

            par2 = path.substring(nose + 1, tail);
            hm.put(par1, par2);

            if (tail == path.indexOf("]")) {
                exit = false;
            } else {
                nose = tail;
                tail = path.indexOf("=", tail);
                exit = true;
            }
        }
        return hm;
    }

     */

/*
    int nose = path.indexOf("enc=");
    int tail = path.indexOf(",");
    String result = path.substring(nose + 4, tail);
        System.out.println("enc=" + result);

    nose = path.indexOf("param1=");
    tail = path.indexOf(",", tail + 1);
    result = path.substring(nose + 7, tail);
        System.out.println("param1=" + result);

    nose = path.indexOf("param2=");
    tail = path.indexOf("]");
    result = path.substring(nose + 7, tail);
        System.out.println("param2=" + result);

*/

/*

        archive.ArrayOfNumbers aon = new archive.ArrayOfNumbers();

        Random random = new Random();

        for (int i = random.nextInt(100); i > 0; i--) {
            aon.add(random.nextInt(21));
        }

        int[] test = new int[10];
        test = aon.get();


        if (test == null) {
            System.out.println("Empty");

        } else {
            for (int i = 0; i < test.length; i++) {
                System.out.println(test[i]);
            }
        }
 */