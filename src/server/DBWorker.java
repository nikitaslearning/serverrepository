package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBWorker {

    private String HOST = "jdbc:postgresql://localhost:5432/cats";
    private final String USERNAME = "postgres";
    private final String PASSWORD = "base";


    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public DBWorker() {
        try {
            connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public DBWorker(String tableName) {
        this.HOST = "jdbc:postgresql://localhost:5432/" + tableName;
        try {
            connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
