package server;

import server.catsClub.*;
import server.catsClub.dao.CatDAO;
import server.catsClub.dao.CatDAOMultiFileImpl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;


public class ServerStarter {

    public static void main(String[] args) {

        int port = 80;
        byte[] buffer = new byte[1000];
        int count;
        boolean run = true;


        CatManager catManager = new CatManager();
        CatDAO catDAO = new CatDAOMultiFileImpl(new File("C:\\Users\\Чёрнаямолния\\Desktop\\Gen3\\server\\src\\server\\dataCats"));
        CatList catList = Config.getCatList();

        Response response = new Response();
        AbstractResponseGenerator generator;
        Error404Generator error404Generator = new Error404Generator();
        CatGenerator catGenerator = new CatGenerator();
        PersonGenerator personGenerator = new PersonGenerator();
        FilesGenerator filesGenerator = new FilesGenerator();
        CurrentTimeGenerator currentTimeGenerator = new CurrentTimeGenerator();
        ListGenerator listGenerator = new ListGenerator();
        AddCatGenerator addCatGenerator = new AddCatGenerator();
        CatDeleteGenerator catDeleteGenerator = new CatDeleteGenerator();
        CatEditFormGenerator catEditGenerator = new CatEditFormGenerator();


            Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("C:\\Users\\Чёрнаямолния\\Desktop\\Gen3\\server\\src\\server\\config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ServerSocket ss = new ServerSocket(port);
            System.out.println(1);



            do {

                Socket socket = ss.accept();

                InputStream sin = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();

                DataInputStream din = new DataInputStream(sin);
                DataOutputStream dout = new DataOutputStream(sout);

                StringBuilder sb = new StringBuilder();

                do {
                    count = din.read(buffer);
                    System.out.println(count);
                    if (count != -1) {
                        sb.append(new String(buffer, 0, count, "ISO-8859-1"));
                    }
                } while (din.available() > 0);

                if (sb.length() > 0) {

                    System.out.println(sb.toString());
                    Request request = new Request(sb.toString());

                    generator = error404Generator;

                    if (request.getPath().startsWith("cat")) {
                        generator = catGenerator;
                    }
                    if (request.getPath().startsWith("person")) {
                        generator = personGenerator;
                    }
                    if (request.getPath().startsWith("file/")) {
                        generator = filesGenerator;
                    }
                    if (request.getPath().startsWith("time")) {
                        generator = currentTimeGenerator;
                    }
                    if (request.getPath().startsWith("list")) {
                        response.htmlText = catManager.listCat(catList.getCats());
                        generator = listGenerator;
                    }
                    if (request.getPath().startsWith("addfarm")) {
                        generator = addCatGenerator;
                    }
                    if (request.getPath().startsWith("addcat")) {
                        catList.addCat(catManager.addCat(catDAO, request));
                        response.htmlText = catManager.listCat(catList.getCats());
                        generator = listGenerator;
                    }
                    if (request.getPath().startsWith("del")) {
                        catDeleteGenerator.catDel(request, catDAO, catList);
                        response.htmlText = catManager.listCat(catList.getCats());
                        generator = listGenerator;
                    }
                    if (request.getPath().startsWith("editfarm")){
                        generator = catEditGenerator;
                    }
                    if (request.getPath().startsWith("editcat")){
                        catEditGenerator.editCat(request, catDAO, catList, catManager);
                        response.htmlText = catManager.listCat(catList.getCats());
                        generator = listGenerator;
                    }
                    generator.generate(request, response);
                    dout.write(response.getFileBody());
                } else {
                    System.out.println("---------------------");
                    System.out.println("------ss.accept------");
                    System.out.println("---------------------");
                    System.out.println();
                    System.out.println();
                }
                socket.close();


                run = true;
            } while (run);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
