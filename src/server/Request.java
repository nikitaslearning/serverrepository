package server;

import java.util.HashMap;

public class Request {

    String method;
    String path;
    String verHTTP;
    String host;
    HashMap parametrs;


    String getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    String getVerHTTP() {
        return verHTTP;
    }

    String getHost() {
        return host;
    }

    HashMap getrHashMap() {
        return parametrs;
    }

    public String getContentType() {
        String contentType = "text/html; charset=UTF-8";

        if (path.endsWith("jpg")) {
            contentType = "image/jpeg";
        }
        if (path.endsWith("js")) {
            contentType = "text/javascript";
        }
        if (path.endsWith(".XML")) {
            contentType = "image/svg+xml";
        }

        return contentType;
    }

    Request(String request) {
        parseRequest(request);
    }

    Request(String method, String path, String verHTTP, String host, HashMap parametrs) {

        this.method = method;
        this.path = path;
        this.verHTTP = verHTTP;
        this.host = host;
        this.parametrs = parametrs;
    }

    Request parseRequest(String request) {

        int par1, par2;

        par1 = 0;
        par2 = request.indexOf(" ");
        this.method = request.substring(par1, par2);


        par1 = par2;
        par2 = request.indexOf(" ", par1 + 1);
        this.path = request.substring(par1 + 2, par2);

        par1 = par2;
        par2 = request.indexOf("\r");
        this.verHTTP = request.substring(par1 + 6, par2);

        par1 = request.indexOf(" ", par2);
        par2 = request.indexOf("\r", par1);
        this.host = request.substring(par1 + 1, par2);

        HashMap parametrs = new HashMap();

        if (request.length() > par2 + 4) {

            boolean exit = true;
            while (exit) {
                String key;
                String value;

                par1 = par2;
                par2 = request.indexOf(":", par1);
                key = request.substring(par1 + 2, par2);

                par1 = par2;
                par2 = request.indexOf("\r", par1);
                value = request.substring(par1 + 2, par2);

                parametrs.put(key, value);

                if (request.length() == par2 + 4) {
                    exit = false;
                }
            }
        }
        Request request1 = new Request(method, path, verHTTP, host, parametrs);
        return request1;
    }
}

