package server;

import java.io.UnsupportedEncodingException;

public class CurrentTimeGenerator extends AbstractResponseGenerator {
    @Override
    public void generate(Request request, Response response) {

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");

        response.htmlText =

                "<h1>Time</h1>\n" +
                        "<p style=\"font-size: 30px\">" + response.getDate("time") + "</p>";

        try {
            response.setFileBody(response.htmlText.getBytes("windows-1251"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
