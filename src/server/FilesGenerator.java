package server;

import server.catsClub.Config;

import java.io.*;

public class FilesGenerator extends AbstractResponseGenerator {

    File baseDir;

    FilesGenerator() {

        this.baseDir = Config.imgPath;
    }

    @Override
    public void generate(Request request, Response response) {

        response.setVerHTTP("HTTP/1.1");
        response.setStatusCode(200);
        response.setReasonFrase("OK");

        response.headers.put("Date: ", response.getDate(""));
        response.headers.put("server: ", "Apache/2.2.14 (Win32)");
        response.headers.put("Last-Modified: ", "Wed, 22 Jul 2009 19:15:56 GMT");
        response.headers.put("Content-Type: ", request.getContentType());
        response.headers.put("Connection: ", "close");
        response.headers.put("Cache-Control: ", "max-age=0, no-cache, must-revalidate, proxy-revalidate");
        response.headers.put("Access-Control-Allow-Origin: ", "*");

        String fileName = request.getPath();
        fileName = fileName.substring(5);

//        читаем сам файл

        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(new File(baseDir, fileName)));

            byte[] buffer = new byte[dis.available()];
            dis.read(buffer);
            dis.close();
//        отправляем и склеиваем (<body>)

            response.setFileBody(buffer);

        } catch (FileNotFoundException e) {
            AbstractResponseGenerator generator = new Error404Generator();
            generator.generate(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}



//            for (int i = 0; dis.available() != 0; i++) {
//
//                if (i == b.length) {
//                    byte[] v = new byte[b.length];
//                    v = b;
//
//                    if (dis.available() > 10) {
//                        b = new byte[b.length + 10];
//                    } else {
//                        b = new byte[b.length + dis.available()];
//                    }
//                    System.arraycopy(v, 0, b, 0, v.length);
//                }
//                b[i] = dis.readByte();
//
//            }