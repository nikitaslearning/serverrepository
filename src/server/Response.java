package server;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Response {


    String verHTTP;
    int statusCode;
    String reasonFrase;

    public HashMap<String, String> headers = new HashMap();

    public String htmlText;
    String messageForClient;

    byte[] fileBody;


    public byte[] getFileBody() {
        byte[] arrayCopy = null;
        int point = 0;

        String messageString = getMessageForClient();
        messageString = messageString + "\n";
        byte[] byteMessageArray = new byte[messageString.length()];
        byteMessageArray = messageString.getBytes();


//        --------------------header settings------------------------
        if (headers.get("Content-Type: ") == "image/jpeg" || headers.get("Content-Type: ") == "text/javascript"
                || headers.get("Content-Type: ") == "image/svg+xml") {

            arrayCopy = new byte[byteMessageArray.length + fileBody.length];
            System.arraycopy(byteMessageArray, 0, arrayCopy, 0, byteMessageArray.length);
            System.arraycopy(fileBody, 0, arrayCopy, byteMessageArray.length, fileBody.length);

        }else {

            arrayCopy = new byte[byteMessageArray.length + fileBody.length + 29];
            System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
            point = byteMessageArray.length;

//         ----------------------body nose------------------------

            messageString = "<html>\n" +
                    "<body>\n";
            byteMessageArray = new byte[14];
            byteMessageArray = messageString.getBytes();
            System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
            point = point + byteMessageArray.length;


//         ----------------------body text------------------------

            System.arraycopy(fileBody, 0, arrayCopy, point, fileBody.length);
            point = point + fileBody.length;

//         ----------------------body tail------------------------
            messageString = "</body>\n" +
                    "</html>";
            byteMessageArray = new byte[16];
            byteMessageArray = messageString.getBytes();
            System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
        }
        return arrayCopy;
    }

    public void setFileBody(byte[] fileBody) {
        this.fileBody = fileBody;

        if (headers.get("Content-Type: ") == "image/jpeg" || headers.get("Content-Type: ") == "text/javascript"
                || headers.get("Content-Type: ") == "image/svg+xml"){
            headers.put("Content-Length: ", Integer.toString(fileBody.length  ));
        }else {
            headers.put("Content-Length: ", Integer.toString(fileBody.length + 29));
        }
    }

    public void setVerHTTP(String verHTTP) {
        this.verHTTP = verHTTP;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setReasonFrase(String reasonFrase) {
        this.reasonFrase = reasonFrase;
    }

    SimpleDateFormat simpleDateFormatForCite= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy hh:mm:ss z", Locale.ENGLISH);

    public String getDate(String format) {

        if (format.equals("time")) {
            return simpleDateFormatForCite.format(new Date());
        }else {
            return simpleDateFormat.format(new Date());
        }
    }

    public String getMessageForClient() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(verHTTP + " " + statusCode + " " + reasonFrase + "\n");

        for (String key : headers.keySet()) {
            stringBuilder.append(key + " " + headers.get(key) + "\n");
        }

//        stringBuilder.append("\n" + htmlText);
        messageForClient = stringBuilder.toString();

        stringBuilder.delete(0, stringBuilder.length());

        return messageForClient;
    }
}

/*

//        --------------------header settings------------------------
        messageString = getMessageForClient();
        int point = 0;


        byte[] byteMessageArray = new byte[messageString.length()];
        byteMessageArray = messageString.getBytes();
        byte[] arrayCopy = new byte[byteMessageArray.length + htmlText.getBytes().length + 31];
        System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
        point = byteMessageArray.length;

//         ----------------------body nose------------------------

        messageString = "\n<html>\n" +
                "<body>\n";
        byteMessageArray = new byte[15];
        byteMessageArray = messageString.getBytes();
        System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
        point = point + byteMessageArray.length;

//         ----------------------body text------------------------

        byteMessageArray = new byte[htmlText.length()];
        try {
            byteMessageArray = htmlText.getBytes("windows-1251");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);
        point = point + byteMessageArray.length;


//         ----------------------body tail------------------------
        messageString = "</body>\n" +
                "</html>";
        byteMessageArray = new byte[16];
        byteMessageArray = messageString.getBytes();
        System.arraycopy(byteMessageArray, 0, arrayCopy, point, byteMessageArray.length);


        return arrayCopy;


 */






/*
----------------------------------------------------------------------------------------------------------------------

//    String date;
//    String server;
//    String lastModified;
//    String contentLength;
//    String contentType;
//    String connection;



//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public void setServer(String server) {
//        this.server = server;
//    }
//
//    public void setLastModified(String lastModified) {
//        this.lastModified = lastModified;
//    }
//
//    public void setContentType(String contentType) {
//        this.contentType = contentType;
//    }
//
//    public void setConnection(String connection) {
//        this.connection = connection;
//    }

//    public void setMessageForClient(String messageForClient) {
//        this.messageForClient = messageForClient;
//    }
//
//
//    public String getVerHTTP() {
//        return verHTTP;
//    }
//
//    public int getStatusCode() {
//        return statusCode;
//    }
//
//    public String getReasonFrase() {
//        return reasonFrase;
//    }

//    public String getServer() {
//        return server;
//    }
//
//    public String getLastModified() {
//        return lastModified;
//    }
//
//    public String getContentLength() {
//        return contentLength;
//    }
//
//    public String getContentType() {
//        return contentType;
//    }
//
//    public String getConnection() {
//        return connection;
//    }

//        messageForClient = verHTTP + " " + statusCode + " " + reasonFrase + "\n" +
//                date + "\n" +
//                server + "\n" +
//                lastModified + "\n" +
//                contentLength + "\n" +
//                contentType + "\n" +
//                connection + "\n" +
//                "\n" +
//                htmlText;
 */


/*
сообщение для клиента
        verHTTP + " " + statusCode + " " + reasonFrase + "\n" +
                date + "\n" +
                server + "\n" +
                lastModified + "\n" +
                contentLength + "\n" +
                contentType + "\n" +
                connection + "\n" +
                "\n" +
                "<html>\n" +
                "<body>\n" +
                "<h1>" + htmlText + "</h1>\n" +
                "</body>\n" +
                "</html>"
 */

/*
--- мой вариант (не правильео понял Вову)

    String response;

    server.Response (server.Request request){
        this.response = pathDistributer(request);
    }

   static String pathDistributer(server.Request request){

        String response;

       if (request.getPath().equals("list")){
           response = "1 ...<br>2 ...<br>3 ...";
       }else {
           response = "no response";
       }
       if (request.getPath().equals("string")){
           response = "test test test test test ...";
       }

       response = "HTTP/1.1 200 OK\n" +
               "Date: Mon, 27 Jul 2009 12:28:53 GMT\n" +
               "server: Apache/2.2.14 (Win32)\n" +
               "Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\n" +
               "Content-Length: 88\n" +
               "Content-Type: text/html\n" +
               "Connection: Closed\n" +
               "\n" +
               "<html>\n" +
               "<body>\n" +
               "<h1>" + response + "</h1>\n" +
               "</body>\n" +
               "</html>";

        return response;
    }


 */