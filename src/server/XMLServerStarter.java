package server;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.catsClub.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class XMLServerStarter {

    public static List<RouteMaping> mapings;

    private static Error404Generator error404Generator = new Error404Generator();

    public static Error404Generator getError404Generator() {
        return error404Generator;
    }


    public static void main(String[] args) {

        Config.getCatList();
        Config.getCatDAO();
        Config.getCatManager();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("C:\\Users\\Чёрнаямолния\\Desktop\\Java\\serverConfig.xml"));
            NodeList controlNodeList = doc.getElementsByTagName("route");
            mapings = new ArrayList<>();

            for (int i = 0; i < controlNodeList.getLength(); i++) {

                NodeList routeChildNodes = controlNodeList.item(i).getChildNodes();
// =================================================================================
//   needed fix bag in "startsWith", if use "equals" img crashed!
// =================================================================================
                String route = routeChildNodes.item(1).getTextContent();
                String calssName = routeChildNodes.item(3).getTextContent();
                Class c = Class.forName(calssName);
                AbstractResponseGenerator generator;
                generator = (AbstractResponseGenerator) c.newInstance();
                RouteMaping routeMaping = new RouteMaping(route, generator);
                mapings.add(routeMaping);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            ServerSocket serverSocket = new ServerSocket(80);

            ArrayBlockingQueue<Socket> arrayBlockingQueue = new ArrayBlockingQueue(10);
            for (int i = 0; i < 4; i++) {
                Thread first = new SimpleThread(arrayBlockingQueue);
                first.start();
            }

            while (true) {
                Socket socket = serverSocket.accept();
                arrayBlockingQueue.add(socket);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class SimpleThread extends Thread {
    ArrayBlockingQueue<Socket> arrayBlockingQueue;

    SimpleThread(ArrayBlockingQueue arrayBlockingQueue) {
        this.arrayBlockingQueue = arrayBlockingQueue;
    }

    @Override
    public void run() {
        while (true) {

            int count;
            byte[] buffer = new byte[1000];

            try {
                Socket socket = arrayBlockingQueue.take();
                InputStream sin = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();
                DataInputStream din = new DataInputStream(sin);
                DataOutputStream dout = new DataOutputStream(sout);
                StringBuilder sb = new StringBuilder();

                do {
                    count = din.read(buffer);

                    if (count != -1) {
                        sb.append(new String(buffer, 0, count, "ISO-8859-1"));
                    }
                } while (din.available() > 0);

                if (sb.length() > 0) {

                    Request request = new Request(sb.toString());
                    System.out.println(sb.toString());
                    Response response = new Response();
                    response.htmlText = Config.getCatManager().listCat(Config.getCatList().getCats());

                    AbstractResponseGenerator generator;
                    generator = XMLServerStarter.getError404Generator();

                    for (int i = 0; i < XMLServerStarter.mapings.size(); i++){
                        if (request.path.startsWith(XMLServerStarter.mapings.get(i).route)){
                            generator = XMLServerStarter.mapings.get(i).generator;
                        }
                    }

                    generator.generate(request, response);
                    dout.write(response.getFileBody());


//                    dout.write(("HTTP/1.1 200 OK\n" +
//                            "Last-Modified:  Wed, 22 Jul 2009 19:15:56 GMT\n" +
//                            "Date:  Thu, 29 Mar 2018 09:27:49 MSK\n" +
//                            "server:  Apache/2.2.14 (Win32)\n" +
//                            "Content-Length:  1269\n" +
//                            "Content-Type:  text/html; charset=UTF-8\n" +
//                            "Cache-Control:  max-age=0, no-cache, must-revalidate, proxy-revalidate\n" +
//                            "Connection:  close\n" +
//                            "\n" +
//                            "<html>\n" +
//                            "<body>\n" +
//                            "<h1>List of Cats</h1><form method=\"get\" action=\"list\"><table border=\"4px\">\n" +
//                            "<tr>\n" +
//                            "<td align=\"center\">1</td><td>Bob</td><td>12 x 7 x 4</td><td align=\"center\"><img src=\"http://localhost/file/cat1.jpg\"</td><td><button type=\"submit\" value=1 name=\"cat\" formaction=\"http://localhost/editform\">Edit</button><button type=\"submit\" value=1 name=\"cat\" formaction=\"del\">Delete</button></tr></p><tr>\n" +
//                            "<td align=\"center\">2</td><td>Silvia</td><td>2 x 3 x 1</td><td align=\"center\"><img src=\"http://localhost/file/cat3.jpg\"</td><td><button type=\"submit\" value=2 name=\"cat\" formaction=\"http://localhost/editform\">Edit</button><button type=\"submit\" value=2 name=\"cat\" formaction=\"del\">Delete</button></tr></p><tr>\n" +
//                            "<td align=\"center\">3</td><td>Tom</td><td>15 x 20 x 10</td><td align=\"center\"><img src=\"http://localhost/file/cat2.jpg\"</td><td><button type=\"submit\" value=3 name=\"cat\" formaction=\"http://localhost/editform\">Edit</button><button type=\"submit\" value=3 name=\"cat\" formaction=\"del\">Delete</button></tr></p></table><p>       <a href=\"http://localhost/addfarm\"><button type=\"button\" name=\"Add Cat\" >Add Cat</button></a>\n" +
//                            "       <input value=\"Delete\" type=\"submit\" formaction=\"del\">       <input value=\"Edit\" type=\"submit\" formaction=\"editform\"></p></form></body>\n" +
//                            "</html>").getBytes());
                }
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getStringNodeValue(NodeList nodeList, String tagName) {
        String nodeValueString = null;

        for (int i = 0; i < nodeList.getLength(); i++) {
            NodeList nl = nodeList.item(i).getChildNodes();
            for (int i1 = 0; i1 < nl.getLength(); i1++) {
                if (tagName.equals(nl.item(i1).getNodeName())) {
                    nodeValueString = nl.item(i1).getTextContent();
                }
            }
        }
        return nodeValueString;
    }
}
