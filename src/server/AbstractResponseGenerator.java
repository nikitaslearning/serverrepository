package server;

public abstract class AbstractResponseGenerator {

    public abstract void generate(Request request, Response response);


}
